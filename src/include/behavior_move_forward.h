#ifndef BEHAVIOR_CROSS_FRAME_H
#define BEHAVIOR_CROSS_FRAME_H

#include <ros/ros.h>
#include <tuple>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>

#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/dronePitchRollCmd.h>

#include <droneMsgsROS/obsVector.h>
#include <droneMsgsROS/Observation3D.h>
#include <yaml-cpp/yaml.h>
#include <aerostack_msgs/BehaviorEvent.h>
#include <droneMsgsROS/droneCommand.h>
#include <behavior_process.h>

#include <std_msgs/Int32.h>

class BehaviorMoveForward : public BehaviorProcess
{
public:
  BehaviorMoveForward();
  ~BehaviorMoveForward();

private:
  const double JUNGLE_TIME = 4.0;


  ros::NodeHandle node_handle;
  double wait_time, pitch_angle;
  ros::Time time;
  bool time_finished;

  /*Config varibles*/
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;

  std::string pitch_roll_out_str;
  std::string command_high_level_str;

  /*Subscribers & Publishers*/
  ros::Publisher pitch_roll_pub;
  ros::Publisher command_high_level_pub;

  ros::Subscriber next_gate_sub;          // Next gate to cross sub

  int next_gate_to_cross_;

private:
  void ownSetUp();
  void ownStart();
  std::tuple<bool, std::string> ownCheckActivationConditions();
  void ownRun();
  void ownStop();

  void timeCallback(const ros::TimerEvent&);
  void nextGateToCrossCallback(const std_msgs::Int32 &msg);

};

#endif
