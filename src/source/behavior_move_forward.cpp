#include "../include/behavior_move_forward.h"


BehaviorMoveForward::BehaviorMoveForward()
{
}

BehaviorMoveForward::~BehaviorMoveForward()
{

}

/*Drone Process*/
void BehaviorMoveForward::ownSetUp()
{
  ros::NodeHandle private_nh("~");
  private_nh.param<std::string>("drone_id", drone_id, "1");
  private_nh.param<std::string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  private_nh.param<std::string>("my_stack_directory", my_stack_directory,
                  "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");

  private_nh.param<std::string>("pitch_roll_out_topic", pitch_roll_out_str, "command/pitch_roll");
  private_nh.param<std::string>("command_high_level_topic", command_high_level_str, "command/high_level");
  ros::param::get("~pitch_angle", pitch_angle);
  ros::param::get("~amount", wait_time);
}

void BehaviorMoveForward::ownStart()
{
  pitch_roll_pub = node_handle.advertise<droneMsgsROS::dronePitchRollCmd>(pitch_roll_out_str, 100);
  command_high_level_pub = node_handle.advertise<droneMsgsROS::droneCommand>(command_high_level_str,1);
  next_gate_sub = node_handle.subscribe("/next_gate_to_cross",1,&BehaviorMoveForward::nextGateToCrossCallback,this);
  setStarted(true);
  time = ros::Time::now() + ros::Duration(wait_time);

  if (next_gate_to_cross_ == 7){
    time = ros::Time::now() + ros::Duration(JUNGLE_TIME);
  }


  std::cout << "Finished ownStart" << '\n';
  ros::param::get("~pitch_angle", pitch_angle);

  // Initialize vairables
  next_gate_to_cross_ = 1;
}

void BehaviorMoveForward::ownRun()
{
  if (ros::Time::now() < time)
  {
      droneMsgsROS::dronePitchRollCmd message;
      pitch_angle = pitch_angle * 0.99;
      message.pitchCmd = -pitch_angle;
      pitch_roll_pub.publish(message);

      droneMsgsROS::droneCommand command_msg;
      command_msg.command = droneMsgsROS::droneCommand::MOVE;
      command_high_level_pub.publish(command_msg);
  }

  else
  {
    droneMsgsROS::dronePitchRollCmd message;
    message.pitchCmd = 0;
    pitch_roll_pub.publish(message);
    sleep(1);
    droneMsgsROS::droneCommand command_msg;
    command_msg.command = droneMsgsROS::droneCommand::HOVER;
    command_high_level_pub.publish(command_msg);

    setFinishEvent(aerostack_msgs::BehaviorEvent::GOAL_ACHIEVED);
    setFinishConditionSatisfied(true);
  }
}

void BehaviorMoveForward::nextGateToCrossCallback(const std_msgs::Int32 &msg){
  next_gate_to_cross_ = msg.data;
}

void BehaviorMoveForward::ownStop()
{
  command_high_level_pub.shutdown();
}

/*Behavior process*/
std::tuple<bool, std::string> BehaviorMoveForward::ownCheckActivationConditions()
{
  return std::make_tuple(true,"");
}
