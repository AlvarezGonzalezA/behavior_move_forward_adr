#include "../include/behavior_move_forward.h"

int main(int argc, char** argv){
  ros::init(argc, argv, ros::this_node::getName());

  std::cout << ros::this_node::getName() << std::endl;

  BehaviorMoveForward behavior;
  behavior.setUp();

  ros::Rate rate(10);
  while(ros::ok()){
    ros::spinOnce();
    behavior.run();
    rate.sleep();
  }

  return 0;
}
