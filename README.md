# Brief
Let the drone moving forward during a time given.
# Published Topics

- **command/high_level** ([droneMsgsROS/droneCommand](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneCommand.msg))  
Publishes the new state of the drone.

- **command/pitch_roll** ([droneCommand/dronePitchRollCmd](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePitchRollCmd.msg))  
Publishes the velocity to reach in the pitch direction.

# Arguments
- **time** (time: X)
Time the drone is going to move forward.

# Diagram

![move forward diagram](img/diagram.png)

# Contributors

Maintainer: Alberto Camporredondo (alberto.camporredondop@alumnos.upm.es)

Author: Alberto Camporredondo (alberto.camporredondop@alumnos.upm.es)
